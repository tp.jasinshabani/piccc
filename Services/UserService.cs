﻿using AutoMapper;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using Domain.Interfaces.Services;
using Domain.Models;
using Microsoft.AspNetCore.Identity;

namespace Services
{
    public class UserService: IUserService
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IUserServiceRepository _userServiceRepository;
        private readonly IMapper _mapper;

        public UserService(
            UserManager<IdentityUser> userManager,
            IMapper mapper,
            IUserServiceRepository userServiceRepository)
        {
            _userManager = userManager;
            _userServiceRepository = userServiceRepository;
            _mapper = mapper;
        }

        public async Task<bool> IsUserWithNameExists(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                return false;
            }
            return true;
        }
 
    }
}

