﻿using AutoMapper;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using Domain.Interfaces.Services;
using Domain.Mappers;
using Domain.Models;

namespace Services
{
    public class EventService : IEventService
    {
        private readonly IEventRepository _eventRepository;
        private readonly IMapper _mapper;

        public EventService(
           IEventRepository eventRepository,
           IMapper mapper)
        {
            _eventRepository = eventRepository;
            _mapper = mapper;
        }

        public async Task<List<EventModel>> GetEvents()
        {
            var events = await _eventRepository.GetEvents();
            return _mapper.Map<List<EventModel>>(events);
        }

        public async Task<EventModel> GetEvent(int id)
        {
            var eventToReturn = await _eventRepository.GetEvent(id);
            var mappedEvent = _mapper.Map<EventModel>(eventToReturn);
            return mappedEvent;
        }

        public async Task<EventModel> AddEvent(EventCreateModel eventToAdd)
        {
            //var datetime = DateTime.Now;
            //var unixTimestampInSeconds = new DateTimeOffset(datetime).ToUnixTimeSeconds();
            var mappedEvent = _mapper.Map<Event>(eventToAdd);
            var createdEvent = await _eventRepository.AddEvent(mappedEvent);
            return _mapper.Map<EventModel>(createdEvent);
        }

        public async Task<EventModel> UpdateEvent(int id, EventCreateModel eventToUpdate)
        {
            var existingEvent = await _eventRepository.GetEvent(id);
            _mapper.Map(eventToUpdate, existingEvent);
            var updatedEvent = await _eventRepository.UpdateEvent(existingEvent);
            return _mapper.Map<EventModel>(updatedEvent);
        }

        public async Task DeleteEvent(int id)
        {
            await _eventRepository.DeleteEvent(id);
        }
    }
}
