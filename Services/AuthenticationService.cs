﻿using Domain.Entities;
using Domain.Interfaces.Services;
using Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _iConfiguration;

        public AuthenticationService(
            UserManager<ApplicationUser> userManager, 
            IConfiguration iConfiguration)
        {
            _userManager = userManager;
            _iConfiguration = iConfiguration;
        }

        public async Task<bool> Login(LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user == null)
                user = await _userManager.FindByEmailAsync(model.Username);

            if (user == null)
            {
                return false;
            }

            var result = await _userManager.CheckPasswordAsync(user, model.Password);
            return result;
        }

        public async Task<UserResponseModel> Authenticate(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                user = await _userManager.FindByEmailAsync(userName);
            }

            var roles = await _userManager.GetRolesAsync(user);
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenKey = Encoding.UTF8.GetBytes(_iConfiguration["JWT:Key"]);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.Sid, user.Id)
                }),
                Expires = DateTime.Now.AddMinutes(60),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return new UserResponseModel
            {
                TokenType = "Bearer",
                AccessToken = tokenHandler.WriteToken(token),
                RefreshToken = "undefined",
                Email = user.Email,
                Id = user.Id,
                Role = (List<string>)roles,
                Username = user.UserName
            };
        }

        public async Task<IdentityResult> CreateUser(CreateUserModel model, UserRolesModel role)
        {
            var user = new ApplicationUser()
            {
                UserName = model.UserName,
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                FirstName = model.FirstName,
                LastName = model.LastName,
                CompanyName = model.ComapanyName,
            };

            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, role.ToString().ToLower());
            }
            return result;
        }
    }
}
