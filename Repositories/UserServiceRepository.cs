﻿using AutoMapper;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using Repositories.DataContext;

namespace Repositories
{
    public class UserServiceRepository : IUserServiceRepository
    {
        private readonly PictorDBContext _context;

        public UserServiceRepository(PictorDBContext context)
        {
            _context = context;
        }

    }
}
