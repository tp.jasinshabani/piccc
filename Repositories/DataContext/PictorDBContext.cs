﻿using Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Repositories.DataContext
{
    public class PictorDBContext: IdentityDbContext<ApplicationUser>
    {
        public PictorDBContext(DbContextOptions<PictorDBContext> options)
             : base(options)
        {
        }
        public DbSet<Event> Event { get; set; } = null!;
    }
}
