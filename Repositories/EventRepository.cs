﻿using AutoMapper;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using Repositories.DataContext;

namespace Repositories
{
    public class EventRepository : IEventRepository
    {
        private readonly PictorDBContext _context;

        public EventRepository(PictorDBContext context)
        {
            _context = context;
        }

        public async Task<Event> AddEvent(Event eventToAdd)
        {
            await _context.Event.AddAsync(eventToAdd);
            await _context.SaveChangesAsync();
            return eventToAdd;
        }

        public async Task DeleteEvent(int id)
        {
            var eventToDelete = await _context.Event.FirstOrDefaultAsync(z => z.Id == id);

            if(eventToDelete != null)
                eventToDelete.IsDeleted = 1;

            await _context.SaveChangesAsync();
        }

        public async Task<Event?> GetEvent(int id)
        {
            return await _context
                .Event
                .Where(x => x.IsDeleted == 0)
                .FirstOrDefaultAsync(z => z.Id == id);
        }

        public async Task<List<Event>> GetEvents()
        {
            return await _context.Event.Where(x => x.IsDeleted == 0).ToListAsync();
        }

        public async Task<Event> UpdateEvent(Event eventToUpdate)
        {
            _context.Update(eventToUpdate);
            await _context.SaveChangesAsync();
            return eventToUpdate;
        }
    }
}
