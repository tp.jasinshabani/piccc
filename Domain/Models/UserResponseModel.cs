﻿namespace Domain.Models
{
	public class UserResponseModel
	{
		public string Id { get; set; } = null!;
		public string AccessToken { get; set; } = null!;
		public string RefreshToken { get; set; } = null!;
		public string Email { get; set; } = null!;
		public string Username { get; set; } = null!;
		public List<string> Role { get; set; } = null!;
		public string TokenType { get; set; } = null!;
		public string[] Errors { get; set; } = null!;
	}
}
