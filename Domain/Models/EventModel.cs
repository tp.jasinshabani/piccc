﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class EventModel
    {
        public long Id { get; set; }
        public string Name { get; set; } = null!;
        public EventType EventType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ClientEmail { get; set; } = null!;
        public long Code { get; set; }
    }

    public class EventCreateModel
    {
        [Required]
        public string Name { get; set; } = null!;
        public EventType EventType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [Required]
        public string ClientEmail { get; set; } = null!;
    }
}
