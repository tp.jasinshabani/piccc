﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class CreateUserModel
    {
        [Required(ErrorMessage = "Username is required")]
        public string UserName { get; set; } = null!;

        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; } = null!;

        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; } = null!;

        [Required(ErrorMessage = "Company name is required")]
        public string ComapanyName { get; set; } = null!;

        [Required(ErrorMessage = "Firstname is required")]
        public string FirstName { get; set; } = null!;

        [Required(ErrorMessage = "Lastname is required")]
        public string LastName { get; set; } = null!;
    }
}
