﻿using Domain.Models;

namespace Domain.Interfaces.Services
{
    public interface IUserService
    {
        Task<bool> IsUserWithNameExists(string userName);
    }
}
