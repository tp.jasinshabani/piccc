﻿using Domain.Entities;
using Domain.Models;

namespace Domain.Interfaces.Services
{
    public interface IEventService
    {
        Task<List<EventModel>> GetEvents();
        Task<EventModel> GetEvent(int id);
        Task<EventModel> AddEvent(EventCreateModel eventToAdd);
        Task<EventModel> UpdateEvent(int id, EventCreateModel eventToUpdate);
        Task DeleteEvent(int id);
    }
}
