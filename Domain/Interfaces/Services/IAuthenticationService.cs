﻿using Domain.Models;
using Microsoft.AspNetCore.Identity;

namespace Domain.Interfaces.Services
{
    public interface IAuthenticationService
    {
        public Task<bool> Login(LoginModel model);
        public Task<UserResponseModel> Authenticate(string userName);
        public Task<IdentityResult> CreateUser(CreateUserModel model, UserRolesModel role);
    }
}
