﻿using Domain.Entities;

namespace Domain.Interfaces.Repositories
{
    public interface IEventRepository
    {
        Task<List<Event>> GetEvents();
        Task<Event?> GetEvent(int id);
        Task<Event> AddEvent(Event eventToAdd);
        Task<Event> UpdateEvent(Event eventToUpdate);
        Task DeleteEvent(int id);

    }
}
