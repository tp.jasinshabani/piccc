﻿using Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    [Table("Event")]
    public class Event
    {
        public long Id { get; set; }
        public string Name { get; set; } = null!;
        public EventType EventType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int IsDeleted { get; set; } = 0;
        public string ClientEmail { get; set; } = null!;
        public long Code { get; set; } = new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds();
    }
}
