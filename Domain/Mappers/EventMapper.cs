﻿using AutoMapper;
using Domain.Entities;
using Domain.Models;

namespace Domain.Mappers
{
    public class EventMapper: Profile
    {
        public EventMapper()
        {
            CreateMap<EventModel, Event>().ReverseMap();
            CreateMap<EventCreateModel, Event>().ReverseMap();
        }
    }
}
