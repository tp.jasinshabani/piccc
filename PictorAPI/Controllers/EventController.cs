﻿using Domain.Entities;
using Domain.Interfaces.Services;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace PictorAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EventController : ControllerBase
    {
        private readonly IEventService _eventService;

        public EventController(IEventService eventService)
        {
            _eventService = eventService;
        }

        [HttpGet]
        [Route("")]
        public async Task<ActionResult<List<EventModel>>> GerEvents()
        {
            var events = await _eventService.GetEvents();
            return Ok(events);
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult> Get(int id)
        {
            var eventToReturn = await _eventService.GetEvent(id);
            return Ok(eventToReturn);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] EventCreateModel eventToAdd)
        {
            var createdEvent = await _eventService.AddEvent(eventToAdd);
            return Ok(createdEvent);
        }

        [HttpPut]
        [Route("{eventId:int}")]
        public async Task<ActionResult<EventModel>> Update(int eventId, [FromBody] EventCreateModel eventToUpdate)
        {
            var updatedEvent = await _eventService.UpdateEvent(eventId, eventToUpdate);
            return Ok(updatedEvent);
        }

        [HttpDelete]
        [Route("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _eventService.DeleteEvent(id);
            return Ok();
        }
    }
}
