﻿using Domain.Interfaces.Services;
using Domain.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Domain.Entities;

namespace PictorAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthenticationController : Controller
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserService _userService;
        private readonly UserManager<IdentityUser> _userManager;

        public AuthenticationController(
            IAuthenticationService authenticationService,
            IUserService userService,
            UserManager<IdentityUser> userManager)
        {
            _authenticationService = authenticationService;
            _userService = userService;
            _userManager = userManager;
        }

        [HttpPost]
        [Route("signin")]
        public async Task<IActionResult> Signin([FromBody] LoginModel model)
        {
            var isLoggedIn = await _authenticationService.Login(model);
            if (isLoggedIn)
            {
                var authenticatedUser = await _authenticationService.Authenticate(model.Username);
                if (authenticatedUser.AccessToken != null)
                {
                    return Ok(authenticatedUser);
                }
            }
            return StatusCode(403);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        [Route("change-password")]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel changePasswordModel)
        {
            var userName = User.FindFirst(ClaimTypes.Name).Value;
            var isUserExists = await _userService.IsUserWithNameExists(userName);
            if (isUserExists)
            {
                var user = await _userManager.FindByNameAsync(userName);
                var changePasswordResult = await _userManager.ChangePasswordAsync(user, changePasswordModel.OldPassword, changePasswordModel.NewPassword);
                if (!changePasswordResult.Succeeded)
                {
                    foreach (var error in changePasswordResult.Errors)
                    {
                        ModelState.TryAddModelError(error.Code, error.Description);
                    }
                    return BadRequest(ModelState);
                }
                return Ok("Password succefully changed!");
            }
            return Unauthorized("User not found, or you dont have access to view this user");
        }
        
        [HttpPost]
        [Route("register-pbo")]
        public async Task<IActionResult> RegisterClient([FromBody] CreateUserModel model)
        {
            var result = await _authenticationService.CreateUser(model, UserRolesModel.PBO);

            string temp = "";
            foreach (var item in result.Errors)
            {
                temp += item.Description + Environment.NewLine;
            }

            if (!result.Succeeded)
            {
                return BadRequest(temp);
            }

            var authenticatedUser = await _authenticationService.Authenticate(model.UserName);
            if (authenticatedUser.AccessToken != null)
            {
                return Ok(authenticatedUser);
            }
            return BadRequest("Something went wrong!");
        }
    }
}
