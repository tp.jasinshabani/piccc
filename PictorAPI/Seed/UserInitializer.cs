﻿using Domain.Entities;
using Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Repositories.DataContext;

namespace PictorAPI.Seed
{
    public class UserInitializer
    {
        public static void InitializeDatabase(IServiceProvider serviceProvider)
        {
            using (var context = new PictorDBContext(
                 serviceProvider.GetRequiredService<
                     DbContextOptions<PictorDBContext>>()))
            {
                var roleManager = serviceProvider.GetService<RoleManager<IdentityRole>>();
                var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();

                if (roleManager != null)
                {
                    if (!roleManager.Roles.Any(r => r.Name == UserRolesModel.SuperAdmin.ToString()))
                    {
                        roleManager.CreateAsync(new IdentityRole(UserRolesModel.SuperAdmin.ToString())).Wait();
                    }

                    if (!roleManager.Roles.Any(r => r.Name == UserRolesModel.PBO.ToString()))
                    {
                        roleManager.CreateAsync(new IdentityRole(UserRolesModel.PBO.ToString())).Wait();
                    }
                }

                if (userManager != null)
                {
                    var user = userManager.FindByEmailAsync("admin@pictor.com");
                    if (user.Result == null)
                    {
                        var password = "Admin123!";
                        var newUser = new ApplicationUser
                        {
                            UserName = "admin@pictor.com",
                            Email = "admin@pictor.com",
                            CompanyName = "Pictor",
                            PhoneNumber = "",
                            FirstName = "",
                            LastName = "",
                            SecurityStamp = Guid.NewGuid().ToString(),
                        };

                        var result = userManager.CreateAsync(newUser, password).Result;

                        if (result.Succeeded)
                        {
                            userManager.AddToRoleAsync(newUser, UserRolesModel.SuperAdmin.ToString().ToLower()).Wait();
                        }
                    }
                }
            }
        }
    }
}
